﻿using UnityEngine;
using System.Collections;

public class enemyAI : MonoBehaviour {

	public float fpsTargetDistance;
	public float enemyLookDistance;
	public float attackDistance;
	public float enemyMovementSpeed;
	public float damping;
	public Transform fpsTarget;
	Rigidbody theRigidBody;
	Renderer myRender;
	
	void Start () {
		myRender = GetComponent<Renderer>();
		theRigidBody = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate(){
		fpsTargetDistance = Vector3.Distance(fpsTarget.position, transform.position);
		if (fpsTargetDistance<enemyLookDistance){
			lookAtPlayer();
			print ("look at player sir");
		}
		if (fpsTargetDistance<attackDistance){
			attack();
		}
	}
	
	void lookAtPlayer(){
		Quaternion rotation = Quaternion.LookRotation(fpsTarget.position - transform.position);
		transform.rotation = Quaternion.Slerp (transform.rotation, rotation, Time.deltaTime*damping);
	}
	
	void attack () {
		theRigidBody.AddForce(transform.forward*enemyMovementSpeed);
	}
}



